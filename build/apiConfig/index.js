/**
 * Auth config.
 */
const devConfig = require('./dev')
const prodConfig = require('./prod')
const env = process.env.NODE_ENV || 'dev'

const apiConfig = {
  // ======================================================
  // Overrides when NODE_ENV === 'development'
  // ======================================================
  dev : devConfig,

  // ======================================================
  // Overrides when NODE_ENV === 'production'
  // ======================================================
  production : prodConfig,
}

module.exports = apiConfig[env]
