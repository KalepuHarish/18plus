/**
 * Production API config.
 */

module.exports = {
  apiBaseUrl: 'https://app.18plus.rocks/',
  tokenName: 'vra-token',
  apiList: {
    auth: {
      register: 'auth/register.json',
      login: 'login',
      validateToken: 'auth/validate_token.json',
      modifyPsw: 'auth/modify_password.json',
      resetPsw: 'auth/reset_password.json'
    },
    me: {
      profile: 'user/',
      message: 'me/message_list.json'
    },
    dash: {
      index: 'dash/dash.json'
    },
    users: {
      index: 'user'
    },
    reports: {
      index: 'reports/report_list.json'
    },
    lotto: {
      index: 'lotto/lotto_list.json'
    },
    settings: {
      index: 'app'
    }
  }
}

