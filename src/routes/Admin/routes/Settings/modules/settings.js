import {
  requestAuthInstance,
  ApiList
} from 'vstore/auth'

/**
 * Constants
 */
export const REQUEST_SETTINGS_POSTS = 'REQUEST_SETTINGS_POSTS'
export const REQUEST_SETTINGS_SUCCESS = 'REQUEST_SETTINGS_SUCCESS'
export const REQUEST_SETTINGS_FAILURE = 'REQUEST_SETTINGS_FAILURE'

/**
 * Actions
 */
export const requestSettingsPosts = () => {
  return {
    type: REQUEST_SETTINGS_POSTS
  }
}

export const requestSettingsSuccess = (data) => {
  return {
    type: REQUEST_SETTINGS_SUCCESS,
    payload: {
      data
    }
  }
}

export const requestSettingsFailure = () => {
  return {
    type: REQUEST_SETTINGS_FAILURE
  }
}

/**
 * Async method
 */
export const fetchSettings = () => {
  return (dispatch) => {
    dispatch(requestSettingsPosts())

    return requestAuthInstance.get(ApiList.settings.index, {
      params: {
        'rnd': (new Date()).getTime()
      }
    })
      .then(res => {
        if (res.data.status === 200) {
          dispatch(requestSettingsSuccess(res.data.data))
        } else {
          dispatch(requestSettingsFailure())
        }
      })
      .catch(err => {
        dispatch(requestSettingsFailure())
        console.log(err)
      })
  }
}

/*  This is a thunk, meaning it is a function that immediately
    returns a function for lazy evaluation. It is incredibly useful for
    creating async actions, especially when combined with redux-thunk! */

export const actions = {
}

/**
 * Action Handlers
 */
const ADMIN_SETTINGS_ACTION_HANDLERS = {
  [REQUEST_SETTINGS_POSTS]: (state) => {
    return ({
      ...state,
      isLoading: true
    })
  },
  [REQUEST_SETTINGS_SUCCESS]: (state, action) => {
    return ({
      ...state,
      isLoading: false,
      appsList: action.payload.data
    })
  },
  [REQUEST_SETTINGS_FAILURE]: (state) => {
    return ({
      ...state,
      isLoading: false
    })
  },
}

/**
 * Reducer
 */
const initialState = {
  isLoading: false,
  appsList: []
}

export default function UserReducer (state = initialState, action) {
  const handler = ADMIN_SETTINGS_ACTION_HANDLERS[action.type]

  return handler ? handler(state, action) : state
}
