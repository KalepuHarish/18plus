import React, { Component } from 'react'
import PropTypes from 'prop-types'
import Helmet from 'react-helmet'
import {
  Table,
  Icon,
  Card,
  Button
} from 'antd'

import './Settings.scss'

class Settings extends Component {
  static propTypes = {
    intl: PropTypes.object,
    isLoading: PropTypes.bool,
    appsList: PropTypes.array
  }

  constructor(props) {
    super(props)
    this.state = {}
  }

  componentWillMount = () => {
    this.props.fetchSettings()
  }

  render() {
    const {
      intl,
      isLoading,
      appsList
    } = this.props

    const { formatMessage } = intl

    const columns = [{
      title: formatMessage({
        id: '_id',
        defaultMessage: 'ID'
      }),
      dataIndex: '_id',
      key: '_id',
      render: text => <a href='#'>{text}</a>,
    },{
      title: formatMessage({
        id: 'image',
        defaultMessage: 'App Logo'
      }),
      dataIndex: 'imageUrl',
      key: 'imageUrl',
      render: text => <img src={text} width='50px' height='50px'/>,
    },{
      title: formatMessage({
        id: 'appid',
        defaultMessage: 'App-ID'
      }),
      dataIndex: 'appId',
      key: 'appId'
    },{
      title: formatMessage({
        id: 'appcontest',
        defaultMessage: 'App Contest'
      }),
      dataIndex: 'appContext',
      key: 'appContext',
    },{
      title: formatMessage({
        id: 'appname',
        defaultMessage: 'App Name'
      }),
      dataIndex: 'appName',
      key: 'appName',
    },{
      title: formatMessage({
        id: 'description',
        defaultMessage: 'Description'
      }),
      dataIndex: 'description',
      key: 'description',
    },{
      title: formatMessage({
        id: 'user.list.action',
        defaultMessage: 'Action'
      }),
      key: 'action',
      render: (text, record) => (
        <span>
          <a href='#'><Icon type='edit' /></a>
          <span className='ant-divider' />
          <a href='#'><Icon type='delete' /></a>
          <span className='ant-divider' />
          <a href='#' className='ant-dropdown-link'>
            Actions <Icon type='down' />
          </a>
        </span>
      ),
    }]

    return (
      <div className='page-layout__wrapper'>
        <Helmet>
          <title>Settings</title>
        </Helmet>
        <div className='settings-wrapper'>
          <Card
            title={<span><Icon type='setting' /> {formatMessage({ id: 'user.list', defaultMessage: 'Apps List' })}</span>}
            noHovering
            bordered={false}
            extra={<Button ghost type='primary' icon='plus'>New App</Button>}>
            <Table
              columns={columns}
              dataSource={appsList}
              loading={isLoading}
              size='middle'
              pagination={{ pageSize: 3 }}
              bordered />
          </Card>
        </div>
      </div>
    )
  }
}

export default Settings
