import { connect } from 'react-redux'
import { siderVisibleChange } from 'vstore/settings'
import { fetchSettings } from '../modules/settings'
import { injectIntl } from 'react-intl'

import Settings from '../components/Settings'

const mapStateToProps = (state) => ({
  isLoading: state.settings.isLoading,
  appsList: state.settings.appsList
})

const mapActionCreators = {
  siderVisibleChange,
  fetchSettings
}

export default connect(mapStateToProps, mapActionCreators)(injectIntl(Settings))
