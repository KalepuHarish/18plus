import { connect } from 'react-redux'

import Users from '../components/Users'

const mapStateToProps = (state) => ({
})

const mapActionCreators = {
}

export default connect(mapStateToProps, mapActionCreators)(Users)
