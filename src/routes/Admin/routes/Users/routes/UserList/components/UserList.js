import React, { Component } from 'react'
import PropTypes from 'prop-types'
import Helmet from 'react-helmet'

import {
  Table,
  Icon,
  Card
} from 'antd'

import './UserList.scss'

class List extends Component {
  static propTypes = {
    intl: PropTypes.object,
    isLoading: PropTypes.bool,
    userList: PropTypes.array,
    fetchList: PropTypes.func
  }

  constructor (props) {
    super(props)
    this.state = {}
  }

  componentWillMount = () => {
    this.props.fetchList()
  }

  render () {
    const {
      intl,
      isLoading,
      userList
    } = this.props

    const { formatMessage } = intl

    const pagination = {
      total : 200
    }

    const columns = [{
      title: formatMessage({
        id: '_id',
        defaultMessage: 'User-ID'
      }),
      dataIndex: '_id',
      key: '_id',
      render: text => <a href='#'>{text}</a>,
    },{
      title: formatMessage({
        id: 'picture',
        defaultMessage: 'Image'
      }),
      dataIndex: 'profile.picture',
      key: 'profile.picture',
      render: text => <img src={text} width='50px' height='50px'/>,
    },{
      title: formatMessage({
        id: 'nickname',
        defaultMessage: 'Nickname'
      }),
      dataIndex: 'nickname',
      key: 'nickname',
    }, {
      title: formatMessage({
        id: 'fullname',
        defaultMessage: 'Full Name'
      }),
      dataIndex: 'profile.name',
      key: 'profile.name',
    },{
      title: formatMessage({
        id: 'mobilenumber',
        defaultMessage: 'Mobile Number'
      }),
      dataIndex: 'profile.mobileNumber',
      key: 'profile.mobileNumber',
    },{
      title: formatMessage({
        id: 'email',
        defaultMessage: 'Email Address'
      }),
      dataIndex: 'email',
      key: 'email',
    },{
      title: formatMessage({
        id: 'age',
        defaultMessage: 'Age'
      }),
      dataIndex: 'profile.age',
      key: 'profile.age',
    },{
      title: formatMessage({
        id: 'user.list.action',
        defaultMessage: 'Action'
      }),
      key: 'action',
      render: (text, record) => (
        <span>
          <a href='#'><Icon type='edit' /></a>
          <span className='ant-divider' />
          <a href='#'><Icon type='delete' /></a>
          <span className='ant-divider' />
          <a href='#' className='ant-dropdown-link'>
            Actions <Icon type='down' />
          </a>
        </span>
      ),
    }]

    return (
      <div className='page-layout__wrapper'>
        <Helmet>
          <title>
            {formatMessage({
              id: 'user.list.title',
              defaultMessage: 'User List'
            })}
          </title>
        </Helmet>
        <div className='user-list-wrapper'>
          <Card
            title={<span><Icon type='team' /> {formatMessage({ id: 'user.list', defaultMessage: 'Users List' })}</span>}
            noHovering
            bordered={false}>
            <Table
              columns={columns}
              dataSource={userList}
              loading={isLoading}
              size='middle'
              pagination={{ pageSize: 3 }}
              bordered />
          </Card>
        </div>
      </div>
    )
  }
}

export default List
