import { connect } from 'react-redux'

import Lotto from '../components/Lotto'

const mapStateToProps = (state) => ({
})

const mapActionCreators = {
}

export default connect(mapStateToProps, mapActionCreators)(Lotto)
