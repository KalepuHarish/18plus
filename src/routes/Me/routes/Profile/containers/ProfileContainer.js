import { connect } from 'react-redux'
import { injectIntl } from 'react-intl'
import { fetchProfile } from '../modules/profile'

import Profile from '../components/Profile'

const mapStateToProps = (state) => ({
  userId: state.auth.userId,
  isLoading: state.profile.isLoading,
  userInfo: state.profile.userInfo,
  //profile: state.profile.userInfo.profile,
})

const mapActionCreators = {
  fetchProfile
}

export default connect(mapStateToProps, mapActionCreators)(injectIntl(Profile))
