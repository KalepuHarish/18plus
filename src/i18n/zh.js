const zhCN = {
  // '': '',
  // Header.
  'header.home': '首页',
  'header.admin': '管理',
  'header.messages': '消息',
  'header.view_all_messages': '查看所有消息',
  'header.profiles': '用户信息',
  'header.password': '密码',
  'header.logout': '退出',

  // Login.
  'login.title': '登录',
  'login.email': '邮箱地址',
  'login.email.required': '请输入您的邮箱地址',
  'login.email.format': '请输入正确格式的邮箱地址',
  'login.password': '输入密码',
  'login.password.required': '请输入您的密码',
  'login.remember_me': '记住我',
  'login.forgot_password': '忘记密码？',
  'login.register': '立即注册',
  'login.register.tips': '还没有账号？',

  // Logout.
  'logout.tips': '成功退出！',
  'logout.redirect': '页面将跳转到',
  'logout.redirect.home_page': '首页',
  'logout.redirect.time': '在',
  'logout.redirect.seconds': '秒之后',
  'logout.redirect.or': '或者点击重新 ',
  'logout.redirect.login': '登录',
  'logout.redirect.again': ' ',

  // Register.
  'register.title': '注册',
  'register.email': '邮箱地址',
  'register.email.required': '请输入您的邮箱地址',
  'register.email.format': '请输入正确格式的邮箱地址',
  'register.password': '输入密码',
  'register.password.required': '请输入您的密码',
  'register.confirm_password': '确认密码',
  'register.password.confirm': '请再次确认您的密码',
  'register.password.consistent': '两次输入的密码不一致',
  'register.nickname': '昵称',
  'register.nickname.required': '请输入您的昵称',
  'register.nickname.tips': '该昵称会显示在系统中并能被其他人看到',
  'register.agreement': '用户协议',
  'register.agreement.tips': '我已阅读并同意该',

  // Reset Password.
  'reset.password': '重置密码',
  'reset.password.email': '邮箱地址',
  'reset.password.email.required': '请输入您的邮箱地址',
  'reset.password.email.format': '请输入正确格式的邮箱地址',
  'reset.password.tips.send': '我们已经发送了一个链接到您的邮件，',
  'reset.password.tips.link': '请通过点击该链接重置您的密码。',

  // Me.
  // Profile.
  'profile': '个人信息',
  'profile.name': '昵称',
  'profile.phone': '手机',
  'profile.email': '邮箱',
  'profile.country': '国家',

  // Modify Password.
  'modify.password.title': '修改密码',
  'modify.password.old': '旧密码',
  'modify.password.old.required': '请输入您的旧密码',
  'modify.password.new': '新密码',
  'modify.password.new.required': '请输入您的新密码',
  'modify.password.confirm_password': '确认新密码',
  'modify.password.confirm': '请再次确认您的密码',
  'modify.password.consistent': '两次输入的密码不一致',

  // Message.
  'message.title': '消息',
  'message.list': '消息列表',
  'message.list.message': '消息',
  'message.list.date': '日期',
  'message.list.status': '状态',
  'message.list.status.read': '已读',
  'message.list.status.unread': '未读',
  'message.list.action': '操作',

  // Admin.
  // Breadcrumb.
  'admin': '管理',
  'dashboard': '概览',
  'users': '用户',
  'list': '列表',
  'total': '交易总表',
  'reports': '报表',
  'lotto': '大乐透',
  'new': '新增',
  'settings': '设置',

  // Sider Bar.
  'admin.users': '用户',
  'admin.users.list': '用户列表',
  'admin.reports': '报表',
  'admin.reports.total': '交易总表',
  'admin.lotto': '大乐透',
  'admin.lotto.list': '乐透列表',
  'admin.settings': '设置',

  // Dashboard
  'dashboard.chart.linear': '线形图',
  'dashboard.chart.bar': '柱状图',
  'dashboard.chart.pie': '饼图',
  'dashboard.chart.radar': '雷达图',

  // User
  // List
  'user.list': '列表',
  'user.list.title': '用户列表',
  'user.list.name': '名称',
  'user.list.age': '年龄',
  'user.list.address': '地址',
  'user.list.action': '操作'
}

export default zhCN
