const enUS = {
  // Header
  'header.home': 'Home',
  'header.admin': 'Admin',
  'header.messages': 'Messages',
  'header.logout': 'Logout',

  // Login
  'login.title': 'Login'
}

export default enUS
