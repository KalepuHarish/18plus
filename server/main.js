const express = require('express')
const path = require('path')
const webpack = require('webpack')
const logger = require('../build/lib/logger')
const webpackConfig = require('../build/webpack.config')
const project = require('../project.config')
const compress = require('compression')
var favicon = require('serve-favicon');
var morgan = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var Database = require('./db/util/database');
var config = require('./util/config.json');
var expressSession = require('express-session');
var flash = require('connect-flash');
const MongoStore = require('connect-mongo')(expressSession);
var ENV = require('./util/env')
var properties = require('./util/properties');

const app = express()
app.use(compress())

var props;
// uncomment after placing your favicon in /public
//app.use(favicon(__dirname + '/public/favicon.ico'));
if (ENV.env.env.trim() === 'dev'){
  console.log("dev")
  props = properties.dev;
}else if (ENV.env.env.trim() === 'qa'){
  console.log("qa")
  props = properties.qa;
}else if (ENV.env.env.trim() === 'production'){
  console.log("production")
  props = properties.production;
}

var dbUrl = props.databaseurl;

app.use(expressSession({secret: '18plus'}));
app.use(expressSession({
  resave: true,
  saveUninitialized: true,
  secret: process.env.SESSION_SECRET,
  store: new MongoStore({
    url: dbUrl,
    autoReconnect: true,
    clear_interval: 3600
  })
}));

app.use(morgan('dev'));
// app.use(bodyParser.json());
// app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json({limit: "50mb"}));
app.use(bodyParser.urlencoded({limit: "50mb", extended: true, parameterLimit:50000}));
app.use(cookieParser());
app.use(flash());

require('./controllers/appgenerator')(app);
require('./controllers/apphome')(app);
require('./controllers/feedLikes')(app);
require('./controllers/heart')(app);
require('./controllers/newsFeed')(app);
require('./controllers/search')(app);
require('./controllers/authentication')(app);
require('./controllers/ImageUpload')(app);

// ------------------------------------
// Apply Webpack HMR Middleware
// ------------------------------------
console.log(project.env)
if (project.env === 'dev') {
  console.log("Harish")
  const compiler = webpack(webpackConfig)

  logger.info('Enabling webpack development and HMR middleware')
  app.use(require('webpack-dev-middleware')(compiler, {
    publicPath  : webpackConfig.output.publicPath,
    contentBase : path.resolve(project.basePath, project.srcDir),
    hot         : true,
    quiet       : false,
    noInfo      : false,
    lazy        : false,
    stats       : 'normal',
  }))
  app.use(require('webpack-hot-middleware')(compiler, {
    path: '/__webpack_hmr'
  }))

  // Serve static assets from ~/public since Webpack is unaware of
  // these files. This middleware doesn't need to be enabled outside
  // of development since this directory will be copied into ~/dist
  // when the application is compiled.
  app.use(express.static(path.resolve(project.basePath, 'public')))

  // This rewrites all routes requests to the root /index.html file
  // (ignoring file requests). If you want to implement universal
  // rendering, you'll want to remove this middleware.
  app.use('*', function (req, res, next) {
    const filename = path.join(compiler.outputPath, 'index.html')
    compiler.outputFileSystem.readFile(filename, (err, result) => {
      if (err) {
        return next(err)
      }
      res.set('content-type', 'text/html')
      res.send(result)
      res.end()
    })
  })
} else {
  logger.warn(
    'Server is being run outside of live development mode, meaning it will ' +
    'only serve the compiled application bundle in ~/dist. Generally you ' +
    'do not need an application server for this and can instead use a web ' +
    'server such as nginx to serve your static files. See the "deployment" ' +
    'section in the README for more information on deployment strategies.'
  )

  // Serving ~/dist by default. Ideally these files should be served by
  // the web server and not the app server, but this helps to demo the
  // server in production.
  app.use(express.static(path.resolve(project.basePath, project.outDir)))
}

Database.config(
  config && config.mongodb && config.mongodb.address ? config.mongodb.address : '', '18plus',
  
  config.mongodb && config.mongodb.options ? config.mongodb.options : undefined,
  function(err, message) {
    if (!err) console.info('  - Mongodb is connected');
    
  }
);

module.exports = app
