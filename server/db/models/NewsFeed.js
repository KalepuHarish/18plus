const mongoose = require('mongoose');
var Schema = mongoose.Schema;
const autopopulate = require('mongoose-autopopulate');

const newsFeedSchema = new mongoose.Schema({
    feedDescription:String,
    feedData: Array,
    feedType: String,
    user: { type: Schema.Types.ObjectId, ref: 'Members', autopopulate: true },
    appId: String
}, { timestamps: true, versionKey: false });

newsFeedSchema.plugin(autopopulate);

const NewsFeed = mongoose.model('NewsFeed', newsFeedSchema);

module.exports = NewsFeed;