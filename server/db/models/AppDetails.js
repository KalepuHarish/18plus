/**
 * Schema Definitions
 */
const mongoose = require('mongoose');
var Schema = mongoose.Schema;
const autopopulate = require('mongoose-autopopulate');

var appDetailsSchema = new Schema({
  appName: {type: String,required: [true,'Kindly enter your appName']},
  description: {type: String,required: [true,'Kindly enter your description']},
  imageUrl:{type: String},
  appId: {type: String},
  appContext: {type: String},
},{versionKey:false,timestamps: true});

appDetailsSchema.plugin(autopopulate);

const AppDetails = mongoose.model('AppDetails', appDetailsSchema);

module.exports = AppDetails;