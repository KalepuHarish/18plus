const mongoose = require('mongoose');
var Schema = mongoose.Schema;
const autopopulate = require('mongoose-autopopulate');

const likesSchema = Schema({
    users: [{ type: Schema.Types.ObjectId, ref: 'Members', autopopulate: true }],
}, { _id: false });

const heartSchema = Schema({
    users: [{ type: Schema.Types.ObjectId, ref: 'Members', autopopulate: true }],
}, { _id: false });

const feedLikesSchema = new mongoose.Schema({
    feed: { type: Schema.Types.ObjectId, ref: 'NewsFeed', autopopulate: true },
    likes: likesSchema,
    heart: heartSchema,
    appId: String
}, { timestamps: true, versionKey: false });

feedLikesSchema.plugin(autopopulate);

const FeedLikes = mongoose.model('FeedLikes', feedLikesSchema);

module.exports = FeedLikes;