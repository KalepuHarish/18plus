var mongoose = require('mongoose');
var ENV = require('../../util/env')
var properties = require('../../util/properties');
var props;
console.log("indb file",ENV.env.env.trim())
if (ENV.env.env.trim() === 'dev'){
  props = properties.dev;
}else if (ENV.env.env.trim() === 'qa'){
  props = properties.qa;
}else if (ENV.env.env.trim() === 'production'){
  props = properties.production;
}
console.log("indb file",props)
var dbUrl = props.databaseurl;

var db = function() {
  var initFlag = false;
  return {

    config: function(addr, dbname, opts, callback) {
      if( !initFlag ){
        var connectUrl = dbUrl;
        mongoose.connect(connectUrl, (opts ? opts : {}));

        var db = mongoose.connection;

        db.on('error', function(err) {
          // Connection Error
          console.log('Mongodb error encountered [' + err + ']');

          if (callback) {
            callback('ERR-MONGODB', 'mongodb - '+err.message);
          }
        });

        db.once('open', function() {
          initFlag = true;
          if (callback) callback(null);
        });
      } else {
        if (callback) callback(null);
      }
    }
  };
};

module.exports = db();