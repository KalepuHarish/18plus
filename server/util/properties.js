exports = module.exports = {
    /*dev details*/
    dev: {
        databaseurl: "mongodb://127.0.0.1:27017/18plus"

    },
    /*qa details*/
    qa: {
        databaseurl: "mongodb://127.0.0.1:27017/18plus"
    },
    /* live */
    production: {
        databaseurl: "mongodb://127.0.0.1:27017/18plus"
    }
}

