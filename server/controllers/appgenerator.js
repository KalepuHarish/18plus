const AppDetails = require('../db/models/AppDetails');
var ObjectId = require('mongodb').ObjectID;
var randomstring = require("randomstring");
var moment = require('moment');

module.exports = function (app) {
    app.post('/app',  function (req, res) {
        var input = req.body;
        const appDetails = new AppDetails(input);
        AppDetails.findOne({ appName: input.appName }, (findErr, existingApp) => {
            if (existingApp) {
                return res.sendStatus(409);
            } else {
                var appId = randomstring.generate(7);
                var appContext = randomstring.generate(7);
                appDetails.appId = appId;
                appDetails.appContext = appContext;
                return appDetails.save((saveErr) => {
                    if (saveErr){ 
						return next(saveErr);
					} else {
						var result = appDetails.toObject()
                        result.createdAt = moment(appDetails.createdAt).format('YYYY-MM-DD hh:mm:ss A');
                        result.updatedAt = moment(appDetails.updatedAt).format('YYYY-MM-DD hh:mm:ss A');
                        res.json({ status: 200, message: "Success", data: result });
					}
                });
            }
        });
    });

    app.get('/app', function (req, res) {
        AppDetails.find({}, function (err, data) {
            if (err)
                res.send(err);
            res.json({ status: 200, message: "Success", data: data });
        });
    });
}
