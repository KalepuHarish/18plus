const FeedLikes = require('../db/models/FeedLikes');
const moment = require('moment');
var ObjectId = require('mongodb').ObjectID;

module.exports = function (app) {
    app.post('/like/:type', function (req, res) {
        var input = req.body;
        var type = req.params.type;
        FeedLikes.findOne({ "feed": ObjectId(input.feedId) }, function (err, data) {
            if (!data) {
                if (type === 'Like') {
                    var feedLikes = new FeedLikes({
                        'feed': input.feedId,
                        'likes.users': [input.userId],
                        'heart.users': [],
                        'appId': input.appId
                    })
                    feedLikes.save(function (err) {
                        if (err) {
                            res.json({ status: 404, message: 'Failure', data: err })
                        } else {
                            var result = { likesCount: feedLikes.likes.users.length, isLiked: true };
                            res.json({ status: 200, message: 'Success', data: result })
                        }
                    });
                } else if (type === 'Heart') {
                    var feedLikes = new FeedLikes({
                        'feed': input.feedId,
                        'likes.users': [],
                        'heart.users': [input.userId],
                        'appId': input.appId
                    })
                    feedLikes.save(function (err) {
                        if (err) {
                            console.error('ERROR!');
                            res.json({ status: 404, message: 'Failure', data: err })
                        } else {
                            var result = { heartCount: feedLikes.heart.users.length, isHeart: true };
                            res.json({ status: 200, message: 'Success', data: result })
                        }
                    });
                }
            } else {
                var flag = false;
                if (type === 'Like') {
                    for (var i = 0; i < data.likes.users.length; i++) {
                        if (String(data.likes.users[i]._id) === input.userId) {
                            flag = true
                            data.likes.users.splice(i, 1);
                        }
                    }
                    if (!flag)
                        data.likes.users.push(input.userId);
                    data.save(function (err) {
                        if (err) {
                            console.error('ERROR!');
                        } else {
                            var result = { likesCount: data.likes.users.length, isLiked: !flag };
                            res.json({ status: 200, message: 'Success', data: result })
                        }
                    });
                } else if (type === 'Heart') {
                    for (var i = 0; i < data.heart.users.length; i++) {
                        if (String(data.heart.users[i]._id) === input.userId) {
                            flag = true
                            data.heart.users.splice(i, 1);
                        }
                    }
                    if (!flag)
                        data.heart.users.push(input.userId);
                    data.save(function (err) {
                        if (err) {
                            console.error('ERROR!');
                        } else {
                            var result = { heartCount: data.heart.users.length, isHeart: !flag };
                            res.json({ status: 200, message: 'Success', data: result })
                        }
                    });
                }
            }
        });
    });
}