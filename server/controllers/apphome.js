const moment = require('moment-timezone');
var bcrypt = require('bcrypt-nodejs');
var nodemailer = require('nodemailer');
var jwt = require('jsonwebtoken');
var NewsFeed = require('../db/models/NewsFeed')
var FeedLikes = require('../db/models/FeedLikes')
var VerifyToken = require('../security/TokenVerification');

module.exports = function (app) {
    app.get('/home/:userId/:appId/:offset/:limit', function (req, res) { //
        console.log("Harish", req.params)
        var appId = req.params.appId;
        var userId = req.params.userId;
        var limit = req.params.limit;
        var offset = req.params.offset;
        NewsFeed.find({ appId: appId }).limit(parseInt(limit)).skip(parseInt(offset)).sort({createdAt:-1}).exec((err, newsFeed) => {
            if (err) {
                console.log('Error in get contests list');
                return res.status(500).send('Something went wrong getting the data');
            } else {
                if (newsFeed) {
                    newsFeed.sort(function (a, b) {
                        return new Date(b.createdAt).getTime() - new Date(a.createdAt).getTime();
                    });
                    var data = [];
                    for (var i = 0; i < newsFeed.length; i++) {
                        var result = newsFeed[i].toObject();
                        result.feedId = newsFeed[i]._id;
                        delete result._id;
                        result.user.userId = newsFeed[i].user._id
                        delete result.user._id
                        delete result.user.tokens
                        delete result.user.password
                        delete result.appId
                        result.createdAt = moment.tz(newsFeed[i].createdAt, 'YYYY-MM-DD hh:mm:ss A', "asia/kolkata").format('YYYY-MM-DD hh:mm:ss A');
                        result.updatedAt = moment.tz(newsFeed[i].updatedAt, 'YYYY-MM-DD hh:mm:ss A', "asia/kolkata").format('YYYY-MM-DD hh:mm:ss A');
                        delete result.user.createdAt;
                        delete result.user.updatedAt;
                        data.push(result);
                    }
                    var count = 0;
                    calculateLikesCount(count, data, req, res)
                }
            }
        });
    });
}

function calculateLikesCount(count, data, req, res) {
    if (count < data.length) {
        var feed = data[count];
        FeedLikes.findOne({ feed: String(feed.feedId) }).exec((err, feedLikes) => {
            if (err) {
                console.log('Error in get contests list');
                return res.status(500).send('Something went wrong getting the data');
            } else {
                if (feedLikes) {
                    var flag = true;
                    var heart = true;
                    for (var i = 0; i < feedLikes.likes.users.length; i++) {
                        if (req.params.userId === String(feedLikes.likes.users[i]._id)) {
                            flag = false;
                            data[count].isLiked = true;
                            break;
                        } else {
                            flag = false;
                            data[count].isLiked = false;
                        }
                    }
                    for (var i = 0; i < feedLikes.heart.users.length; i++) {
                        if (req.params.userId === String(feedLikes.heart.users[i]._id)) {
                            heart = false;
                            data[count].isHeart = true;
                            break;
                        } else {
                            heart = false;
                            data[count].isHeart = false;
                        }
                    }
                    if (flag) data[count].isLiked = false;
                    if (heart) data[count].isHeart = false;
                    data[count].likesCount = feedLikes.likes.users.length;
                    data[count].heartCount = feedLikes.heart.users.length;
                } else {
                    data[count].isLiked = false;
                    data[count].likesCount = 0;
                    data[count].isHeart = false;
                    data[count].heartCount = 0;
                }
                count = count + 1;
                calculateLikesCount(count, data, req, res);
            }
        })
    } else {
        return res.json({ status: 200, message: "Success", data: data });
    }
}