const NewsFeed = require('../db/models/NewsFeed');
const moment = require('moment');
var cloudinary = require('cloudinary');

cloudinary.config({
    cloud_name: 'plus18',
    api_key: '125318575746195',
    api_secret: 'jk9HC4cO5-sOeX2dts0FNf0mDDo'
})

module.exports = function (app) {
    app.post('/feed', function (req, res) {
        const newsFeed = new NewsFeed(req.body);
        var result = [];
        var count = 0;
        imageUpload(count,result,newsFeed,res);
    });


    app.put('/feed/:feedId', function (req, res) {
        NewsFeed.findOneAndUpdate({ _id: req.params.feedId }, req.body, { new: true }, function (err, data) {
            if (err)
                res.send(err);
            if (data) {
                var result = data.toObject()
                result.feedId = data._id;
                delete result._id;
                result.user.userId = data.user._id;
                delete result.user._id;
                result.createdAt = moment(data.createdAt).format('YYYY-MM-DD hh:mm:ss A');
                result.updatedAt = moment(data.updatedAt).format('YYYY-MM-DD hh:mm:ss A');
                result.user.createdAt = moment(data.user.createdAt).format('YYYY-MM-DD hh:mm:ss A');
                result.user.updatedAt = moment(data.user.updatedAt).format('YYYY-MM-DD hh:mm:ss A');
                delete result.user.password;
                delete result.user.tokens;
                res.json({ status: 200, message: "Success", data: result });
            }
        });
    });

    app.get('/feed/:feedId', function (req, res) {
        NewsFeed.findById(req.params.feedId, function (err, data) {
            if (err) {
                res.send(err);
            } else {
                var result = data.toObject()
                result.feedId = data._id;
                delete result._id;
                result.user.userId = data.user._id;
                delete result.user._id;
                result.createdAt = moment(data.createdAt).format('YYYY-MM-DD hh:mm:ss A');
                result.updatedAt = moment(data.updatedAt).format('YYYY-MM-DD hh:mm:ss A');
                result.user.createdAt = moment(data.user.createdAt).format('YYYY-MM-DD hh:mm:ss A');
                result.user.updatedAt = moment(data.user.updatedAt).format('YYYY-MM-DD hh:mm:ss A');
                delete result.user.password;
                delete result.user.tokens;
                res.json({ status: 200, message: "Success", data: result });
            }
        });
    });

    app.get('/share/:feedId', function (req, res) {
        NewsFeed.findById(req.params.feedId, function (err, data) {
            if (err) {
                res.send(err);
            } else {
                var result = "<html prefix=\"og: http://ogp.me/ns#\">"
                +"<head><title>The Rock (1996)</title>"
                +"<meta property=\"og:url\"                content=\""+"https://app.18plus.rocks"+"\" />"
                +"<meta property=\"og:type\"               content=\"place\" />"
                +"<meta property=\"og:title\"              content=\"When Great Minds Don’t Think Alike\" />"
                +"<meta property=\"og:description\"        content=\"How much does culture influence creative thinking?\" />"
                +"<meta property=\"og:image\"              content=\""+data.feedData[0]+"\" />"
                +"</head></html>" 
                res.send(result);
            }
        });
    });

    app.get('/data/:feedId', function (req, res) {
        NewsFeed.findById(req.params.feedId, function (err, data) {
            if (err) {
                res.send(err);
            } else {
                var result = "<html prefix=\"og: http://ogp.me/ns#\">"
                +"<head><title>The Rock (1996)</title>"
                +"<meta property=\"og:url\"                content=\""+"https://app.18plus.rocks"+"\" />"
                +"<meta property=\"og:type\"               content=\"place\" />"
                +"<meta property=\"og:title\"              content=\"When Great Minds Don’t Think Alike\" />"
                +"<meta property=\"og:description\"        content=\"How much does culture influence creative thinking?\" />"
                +"<meta property=\"og:image\"              content=\""+data.feedData[0]+"\" />"
                +"</head></html>" 
                res.send(result);
            }
        });
    });


    app.post('/feed/:feedId', function (req, res) {
        NewsFeed.remove({
            _id: req.params.feedId
        }, function (err, user) {
            if (err)
                res.send(err);
            res.json({ status: 200, message: 'Feed successfully deleted' });
        });
    });

}

function imageUpload(count,response,newsFeed,res) {
    if (count < newsFeed.feedData.length) {
        cloudinary.uploader.upload("data:image/png/gif;base64," + newsFeed.feedData[count], function (result) {
            console.log(result.url)
            response.push(result.url)
            count = +count + 1;
            imageUpload(count,response,newsFeed,res)
        });
    } else {
        if(response.length>0){
            newsFeed.feedData = response;
        }
        newsFeed.save((err) => {
            if (err) { return next(err); }
            NewsFeed.findById(newsFeed._id, function (err, data) {
                if (data) {
                    var result = data.toObject()
                    result.feedId = data._id;
                    delete result._id;
                    result.user.userId = data.user._id;
                    delete result.user._id;
                    result.createdAt = moment(data.createdAt).format('YYYY-MM-DD hh:mm:ss A');
                    result.updatedAt = moment(data.updatedAt).format('YYYY-MM-DD hh:mm:ss A');
                    delete result.user.createdAt;
                    delete result.user.updatedAt;
                    delete result.user.password;
                    delete result.user.tokens;
                    delete result.appId
                    res.json({ status: 200, message: "Success", data: result });
                }
            });
        });
    }
}