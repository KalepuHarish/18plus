const NewsFeed = require('../db/models/NewsFeed');
const FeedLikes = require('../db/models/FeedLikes');
var ObjectId = require('mongodb').ObjectID;
const moment = require('moment-timezone');
/**
 * GET /createNewApp
 */
module.exports = function (app) {
    app.get('/heart/:userId/:appId/:offset/:limit', function (req, res) {
        var appId = req.params.appId;
        var userId = req.params.userId;
        var limit = req.params.limit;
        var offset = req.params.offset;
        FeedLikes.find({ appId: appId, "heart.users": { "$in": [ObjectId(userId)] } }).limit(parseInt(limit)).skip(parseInt(offset)).sort({'feed.createdAt':-1}).exec((err, info) => {
            if (err) {
                console.log('Error in get contests list');
                return res.status(500).send('Something went wrong getting the data');
            } else {
                if (info) {
                    info.sort(function (a, b) {
                        return new Date(b.createdAt).getTime() - new Date(a.createdAt).getTime();
                    });
                    var data = [];
                    for (var i = 0; i < info.length; i++) {
                        var result = info[i].toObject();
                        delete result._id
                        delete result.appId
                        delete result.createdAt
                        delete result.updatedAt
                        result.feed.feedId = info[i].feed._id;
                        result.feed.createdAt = moment.tz(info[i].feed.createdAt, 'YYYY-MM-DD hh:mm:ss A', "asia/kolkata").format('YYYY-MM-DD hh:mm:ss A');
                        result.feed.updatedAt = moment.tz(info[i].feed.updatedAt, 'YYYY-MM-DD hh:mm:ss A', "asia/kolkata").format('YYYY-MM-DD hh:mm:ss A');
                        delete result.feed._id;
                        delete result.feed.appId
                        //User Info Under Feed
                        result.feed.user.userId = info[i].feed.user._id
                        delete result.feed.user._id
                        delete result.feed.user.tokens
                        delete result.feed.user.password
                        delete result.feed.user.appId
                        delete result.feed.user.createdAt;
                        delete result.feed.user.updatedAt;
                        //User Info Under Likes
                        for (var k = 0; k < info[i].likes.users.length; k++) {
                            result.likes.users[k].userId = info[i].likes.users[k]._id
                            delete result.likes.users[k]._id
                            delete result.likes.users[k].tokens
                            delete result.likes.users[k].password
                            delete result.likes.users[k].appId
                            delete result.likes.users[k].createdAt;
                            delete result.likes.users[k].updatedAt;
                        }
                        //User Info Under Heart
                        for (var k = 0; k < info[i].heart.users.length; k++) {
                            result.heart.users[k].userId = info[i].heart.users[k]._id
                            delete result.heart.users[k]._id
                            delete result.heart.users[k].tokens
                            delete result.heart.users[k].password
                            delete result.heart.users[k].appId
                            delete result.heart.users[k].createdAt;
                            delete result.heart.users[k].updatedAt;
                        }
                        data.push(result);
                    }
                    var count = 0;
                    calculateLikesCount(count, data, req, res)
                }
            }
        });
    });
}

function calculateLikesCount(count, data, req, res) {
    if (count < data.length) {
        var feed = data[count].feed;
        FeedLikes.findOne({ feed: String(feed.feedId) }).exec((err, feedLikes) => {
            if (err) {
                console.log('Error in get contests list');
                return res.status(500).send('Something went wrong getting the data');
            } else {
                if (feedLikes) {
                    var flag = true;
                    var heart = true;
                    for (var i = 0; i < feedLikes.likes.users.length; i++) {
                        if (req.params.userId === String(feedLikes.likes.users[i]._id)) {
                            flag = false;
                            data[count].feed.isLiked = true;
                            break;
                        } else {
                            flag = false;
                            data[count].feed.isLiked = false;
                        }
                    }
                    for (var i = 0; i < feedLikes.heart.users.length; i++) {
                        if (req.params.userId === String(feedLikes.heart.users[i]._id)) {
                            heart = false;
                            data[count].feed.isHeart = true;
                            break;
                        } else {
                            heart = false;
                            data[count].feed.isHeart = false;
                        }
                    }
                    if (flag) data[count].feed.isLiked = false;
                    if (heart) data[count].feed.isHeart = false;
                    data[count].feed.likesCount = feedLikes.likes.users.length;
                    data[count].feed.heartCount = feedLikes.heart.users.length;
                } else {
                    data[count].feed.isLiked = false;
                    data[count].feed.likesCount = 0;
                    data[count].feed.isHeart = false;
                    data[count].feed.heartCount = 0;
                }
                count = count + 1;
                calculateLikesCount(count, data, req, res);
            }
        })
    } else {
        return res.json({ status: 200, message: "Success", data: data });
    }
}
