const NewsFeed = require('../db/models/NewsFeed');
const FeedLikes = require('../db/models/FeedLikes');
const Users = require('../db/models/members');
var ObjectId = require('mongodb').ObjectID;
var moment = require('moment');

module.exports = function (app) {
    app.get('/search/:userId/:appId', function (req, res) {
        var appId = req.params.appId;
        var userId = req.params.userId;
        NewsFeed.find({ appId: appId }).exec((err, newsFeed) => {
            if (err) {
                console.log('Error in get contests list');
                return res.status(500).send('Something went wrong getting the data');
            } else {
                var result = [];
                for (var i = 0; i < newsFeed.length; i++) {
                    for (var n = 0; n < newsFeed[i].feedData.length; n++) {
                        result.push(newsFeed[i].feedData[n]);
                    }
                }
                return res.json({ status: 200, message: "Success", data: result });
            }
        });
    });

    app.get('/searchByUser/:userId/:appId', function (req, res) {
        var appId = req.params.appId;
        var userId = req.params.userId;
        Users.findOne({ appId: appId, _id: ObjectId(userId) }).exec((err, user) => {
            if (err) {
                console.log('Error in get contests list');
                return res.status(500).send('Something went wrong getting the data');
            } else {
                NewsFeed.find({ appId: appId, user: ObjectId(userId) }).exec((err, newsFeed) => {
                    if (err) {
                        console.log('Error in get contests list');
                        return res.status(500).send('Something went wrong getting the data');
                    } else {
                        var data = [];
                        for (var i = 0; i < newsFeed.length; i++) {
                            var result = newsFeed[i].toObject();
                            result.feedId = newsFeed[i]._id;
                            delete result._id;
                            result.user.userId = newsFeed[i].user._id
                            delete result.user._id
                            delete result.user.tokens
                            delete result.user.password
                            delete result.appId
                            result.createdAt = moment(newsFeed[i].createdAt).format('YYYY-MM-DD hh:mm:ss A');
                            result.updatedAt = moment(newsFeed[i].updatedAt).format('YYYY-MM-DD hh:mm:ss A');
                            delete result.user.createdAt;
                            delete result.user.updatedAt;
                            data.push(result);
                            data.sort(function (a, b) {
                                var aDate = moment(a.createdAt, 'YYYY-MM-DD hh:mm:ss').toDate();
                                var bDate = moment(b.createdAt, 'YYYY-MM-DD hh:mm:ss').toDate();
                                return bDate - aDate;
                            });
                        }
                        var count = 0;
                        calculateLikesCount1(count, data, req, res, user)
                    }
                });
            }
        });
    });
}

function calculateLikesCount1(count, data, req, res, user) {
    if (count < data.length) {
        var feed = data[count];
        FeedLikes.findOne({ feed: String(feed.feedId) }).exec((err, feedLikes) => {
            if (err) {
                console.log('Error in get contests list');
                return res.status(500).send('Something went wrong getting the data');
            } else {
                if (feedLikes) {
                    var flag = true;
                    var heart = true;
                    for (var i = 0; i < feedLikes.likes.users.length; i++) {
                        if (req.params.userId === String(feedLikes.likes.users[i]._id)) {
                            flag = false;
                            data[count].isLiked = true;
                            break;
                        } else {
                            flag = false;
                            data[count].isLiked = false;
                        }
                    }
                    for (var i = 0; i < feedLikes.heart.users.length; i++) {
                        if (req.params.userId === String(feedLikes.heart.users[i]._id)) {
                            heart = false;
                            data[count].isHeart = true;
                            break;
                        } else {
                            heart = false;
                            data[count].isHeart = false;
                        }
                    }
                    if (flag) data[count].isLiked = false;
                    if (heart) data[count].isHeart = false;
                    data[count].likesCount = feedLikes.likes.users.length;
                    data[count].heartCount = feedLikes.heart.users.length;
                } else {
                    data[count].isLiked = false;
                    data[count].likesCount = 0;
                    data[count].isHeart = false;
                    data[count].heartCount = 0;
                }
                count = count + 1;
                calculateLikesCount1(count, data, req, res, user);
            }
        })
    } else {
        return res.json({ status: 200, message: "Success", data: { userInfo: user, userFeeds: data } });
    }
}